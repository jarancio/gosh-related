# Measuring openness

Within open science in general, different authors have studied how openness varies in the different stages of research: design, collection, analysis, conclusions / communication ([RIN/NESTA 2010](http://www.rin.ac.uk/system/files/attachments/NESTA-RIN_Open_Science_V01_0.pdf)). But when we look into hardware, there are not many studies on the openness except for very few authors and in specific domains (such as non-electronic designs, [Bonvoisin et al 2017](https://openhardware.metajnl.com/articles/10.5334/joh.7/))

## Why?

Openness within GOSH initiatives is not homogeneous. 

- Most of the projects focus on openness in the last stage, open designs, editable files in the best cases, but few challenge the closed design logic and open the process to external actors. Many times we see that it is not enough just to make the material available. They're open but not **accessible**

- There is a risk of taking the concept of "openness" to implement mechanisms that do not challenge the current logic but that become functional to the status quo instead

- Learning: Why are some projects more replicated than others?

- Understanding differences between projects: open doesn't mean the same to all, visibilizing and learning from differences 

## Tools to evaluate projects

Bonvoisin et al (2017) studied some components of the openness in a particular kind of open hw projects. Based on product and process openness (from Aitamurto et al, 2015), they propose a global openness index that analyzes the documentation of the projects around the four freedoms. The higher the index, the better.

Product openness
- Transparency (study)
- Replicability (modification)
- Free for commercial use (use)

Process openness
- Accessibility (use - are the editable files available?)

## Expanding the Global Openness Index (in progress)

Can we expand this index in terms of processes openness? Can we operationalize the concept of openness to serve the vision of GOSH community? 

Product openness
- Transparency (study - are the schematics, the design files available?)
- Replicability (replication - BoM, assembly instructions?)
- Free for commercial use (use)

Process openness 
- Accessibility (modification - are the editable files available?)
- Quality of documentation (languages, various formats?)
- Diversity of collaborators (academy / non-academy, non-conforming identities, gender)
- Decision-making mechanisms (horizontal, centralized)

## Some questions

- How can we better complete this index? What do we need to make it local/contextualized to academic backgrounds vs community backgrounds for example?

- Are there any changes in the index with time during the lifetime of a project? 

- Are there any significant geographical differences once the index is calculated?

- Can it be useful as a guideline for building good practices towards increasing openness in GOSH projects?

- Some components of the index are usually more developed than others, why? What additional efforts and resources require process axes? 

[Protopresentation at Tecnox 2018, Chile](https://docs.google.com/presentation/d/15D8y_FvX8wHhSHz71zm9Frl2HYKDMlLpCxC63X646As/edit?usp=sharing)